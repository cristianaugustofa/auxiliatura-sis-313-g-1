export default function Button (props:{title:string,red?:boolean}){
    return(
        props.red?
        <button className="red">{props.title}</button>
        :
        <button>{props.title}</button>
    )
}